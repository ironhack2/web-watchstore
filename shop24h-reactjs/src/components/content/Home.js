import { Container, Grid, Card, CardActionArea, CardMedia, CardContent, Typography, Button, } from "@mui/material";
import { NavLink } from "react-router-dom";

import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";


function Home() {
  const dispatch = useDispatch();
  const { allProduct } = useSelector((reduxData) => reduxData.taskReducer);

  const fetchAPI = async (url) => {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  };

  useEffect(() => {
    fetchAPI("http://localhost:8000/products/?limit=8")
      .then((data) => {
        dispatch({
          type: "ALL_PRODUCT",
          setProducts: data.data,
        });
        console.log(data);
      })
      .catch((error) => {
        console.error(error.message);
      });
  }, []);


  return (
    <Container>
      {/* Title */}
      <Container>
        <Grid item xs={12} mt={6} p={2}>
          <Typography variant="h6"><b>LATEST PRODUCT</b></Typography>
        </Grid>
      </Container>

      {/* ////////  * PRODUCT *    ////////////// */}
      <Container>
        <Grid container>
          {allProduct.map((product, index) => {
            return (
              <Grid item xs={12} lg={3} md={3} sm={6} mb={3} className="p-3" key={index}>
                <NavLink to={product._id} style={{ textDecoration: "none" }}>
                  <div>
                    {/* <Card> */}
                    <CardActionArea>
                      <CardMedia
                        component="img"
                        // height="140"
                        width="200"
                        image={product.imageUrl}
                        alt="green iguana"
                        className="img-card"
                      />
                      <CardContent>
                        <Typography
                          style={{ fontSize: "16px", color: "#000" }}
                          component="div"
                          className="name-product"
                          align="center"
                        >
                          <b>{product.name}</b>
                        </Typography>

                        <Typography mt={2} variant="body1" align="center" color="text.secondary">
                          <strike>Giá cũ: ${product.buyPrice}</strike>
                        </Typography>

                        <Typography variant="h6" align="center" sx={{ color: "red" }}>
                          <b>Giá mới: ${product.promotionPrice}</b>
                        </Typography>
                      </CardContent>
                    </CardActionArea>
                    {/* </Card> */}
                  </div>
                </NavLink>
              </Grid>
            );
          })}
        </Grid>
      </Container>

      <Grid item xs={12} style={{ textAlign: "center" }} mt={5}>
        <NavLink to="products" style={{ textDecoration: "none" }}>
          <Button
            variant="contained"
            style={{ backgroundColor: "#000" }}
          >
            Show All
          </Button>
        </NavLink>
      </Grid>
    </Container>
  );
}

export default Home;
