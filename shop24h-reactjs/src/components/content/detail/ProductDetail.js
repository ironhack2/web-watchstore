import { Container, Grid, Typography, Button, Rating } from "@mui/material";

import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import MonetizationOnIcon from "@mui/icons-material/MonetizationOn";
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';

import { useDispatch, useSelector } from "react-redux"

function ProductDetail() {
  //Link đến trang theo ID
  const { productId } = useParams();

  const { cart } = useSelector((reduxData) => reduxData.cartReducer);

  let getCount = localStorage.getItem(productId) ?? 0
  const [count, setCount] = useState(parseInt(getCount));

  const [productInfo, setProductInfo] = useState({});


  //Tính tiền
  const [quantity, setQuantity] = useState(0);
  const [bill, setBill] = useState(0);

  const dispatch = useDispatch();

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }


  const btnAddCart = () => {
    var cartCount = parseInt(cart) + quantity
    setCount(count + quantity);
    localStorage.setItem(productId, count)
    for (let i = 0; i < quantity; i++) {
      dispatch({
        type: "ADD_CART",
        cart: cartCount,
        id: productInfo._id,
      })
    }
    localStorage.setItem("cart", cartCount)
  };

  //Tính tiền
  const minusQuantity = () => {
    setQuantity(quantity === 0 ? 0 : quantity - 1);
  };

  const plusQuantity = () => {
    setQuantity(quantity + 1);
  };

  // Load API
  const fetchAPI = async (url) => {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  };

  useEffect(() => {
    fetchAPI("http://localhost:8000/products/" + productId)
      .then((data) => {
        console.log(data);
        setProductInfo(data.data)
      })
      .catch((error) => {
        console.error(error.message);
      });
    //Tính tiền
    setBill(quantity === 0 ? 0 : quantity * productInfo.promotionPrice);
  }, [quantity]);

  localStorage.setItem(productId, count)

  return (
    <Container style={{ backgroundColor: "#ffffff" }}>
      <Grid container mt={5} p={3}>
        <Grid item xs={5} >
          <img className="img-card"
            src={productInfo.imageUrl}
            style={{ width: "80%", borderRadius: "14px" }}
          />
        </Grid>

        <Grid item xs={1}>
        </Grid>

        <Grid item xs={6}>
          <Grid container>
            <Grid item xs={12}>
              <Typography variant="h5">
                <b>{productInfo.name}</b>
              </Typography>
            </Grid>
          </Grid>

          <Grid container mt={3}>
            <Grid item xs={12}>
              <Rating
                name="half-rating-read"
                defaultValue={5}
                precision={0.5}
                readOnly
              />
            </Grid>
          </Grid>

          <Grid container mt={5}>
            <Grid item xs={12}>
              <Typography variant="body1">
                <strike>
                  <b style={{ opacity: 0.7 }}>Giá cũ: ${productInfo.buyPrice}</b>
                </strike>
              </Typography>
            </Grid>
          </Grid>

          <Grid container mt={1}>
            <Grid item xs={12}>
              <Typography variant="h6">
                <b style={{ color: "red" }}>Giá mới: ${productInfo.promotionPrice}</b>
              </Typography>
            </Grid>
          </Grid>

          <Grid container mt={2}>
            <Grid item xs={12}>
              <Typography variant="body1">
                <b style={{ color: "#009688" }}>Số lượng: {productInfo.amount} sản phẩm</b>
              </Typography>
            </Grid>
          </Grid>

          <Grid item xs={12} mt={5}>
            <Grid container>
              <Grid item xs={12} md={3} sm={6} lg={3}>
                <Grid container>
                  <Grid item xs={3} align="right">
                    <Typography variant="h4" mt={1}>
                      {quantity}
                    </Typography>
                  </Grid>

                  <Grid item xs={9} sm={6}>
                    <Button onClick={plusQuantity} size="small" sx={{ color: "black" }}>
                      <AddCircleIcon />
                    </Button>

                    <Button onClick={minusQuantity} size="small" sx={{ color: "black" }}>
                      <RemoveCircleIcon />
                    </Button>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={12} lg={9} sm={4} md={9} mt={1}>
                <Button variant="contained" color="success" onClick={btnAddCart}>
                  <b>Add cart</b>
                </Button>
              </Grid>
            </Grid>
          </Grid>

          <Grid p={1} mt={2} mb={5} item xs={12} md={6} lg={6} sm={12}
            style={{
              color: "#000",
              border: "2px solid #000"
            }}
          >
            <Typography align="center" variant="h6">
              <b>TOTAL:
                &nbsp; <span>${bill.toLocaleString()}</span>
              </b>
            </Typography>
          </Grid>
        </Grid>
      </Grid>

      <Grid container mt={8} className="p-3" style={{ borderRadius: "14px" }}>
        <Grid xs={12}>
          <Typography variant="h5">
            <b>Description</b>
          </Typography>
        </Grid>

        <Grid item xs={12} lg={8} sm={12} md={8} mt={2} pb={5}>
          <Typography variant="body1">{productInfo.description}</Typography>
        </Grid>
      </Grid>
    </Container>
  );
}

export default ProductDetail;
