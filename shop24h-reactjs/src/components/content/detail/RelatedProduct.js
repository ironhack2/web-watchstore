import { Container, Grid, Link, Button, Typography } from "@mui/material";
import { useState, useEffect } from "react";

function RelatedProject() {

    const [relatedProject, setRelatedProject] = useState([]);

    const fetchAPI = async (url) => {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    }

    useEffect(() => {
        fetchAPI("http://localhost:8000/products/?limit=6&skip=4")
            .then((data) => {
                setRelatedProject(data.data);
            })
            .catch((error) => {
                console.log(error);
            })
    }, [])


    return (
        <Container>
            <Typography mt={5} variant="h6">
                <b>Sản phẩm bạn có thể yêu thích</b>
            </Typography>
            <Grid container mt={2}>
                {
                    relatedProject.map((product, index) => {
                        return (
                            <Grid item xs={2}>
                                <Link href={product._id}>
                                    <Button>
                                        <img src={product.imageUrl} style={{ width: "98%" }} className="img-card" />
                                    </Button>
                                </Link>
                            </Grid>
                        )
                    })
                }

            </Grid>
        </Container>
    )
}

export default RelatedProject;