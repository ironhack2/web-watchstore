import firebase from "firebase";

import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyBpqPwJP3lp42F6gdgftrieOFx6hc4au5c",
    authDomain: "shop-24h-999.firebaseapp.com",
    projectId: "shop-24h-999",
    storageBucket: "shop-24h-999.appspot.com",
    messagingSenderId: "683887426367",
    appId: "1:683887426367:web:30a597373729a17b16f18a"
}

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const googleProvider = new firebase.auth.GoogleAuthProvider();