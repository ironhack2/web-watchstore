import Header from "../components/Header-Footer/Header";
import BreadCrumb from "../components/Header-Footer/BreadCrumb";
import Footer from "../components/Header-Footer/Footer";

import ProductFilter from "../components/content/products/ProductFilter";
import ProductsContent from "../components/content/products/Products";

import { Container, Grid } from "@mui/material";

const breadCrumbs = [
  {
    name: "Trang chủ",
    route: "/",
  },
  {
    name: "trang sản phẩm",
    route: "/products",
  }
]

function ProductsPage() {
  return (
    <Container>
      <Header />
      <BreadCrumb breadCrumbs={breadCrumbs} />

      {/* Fillter  */}
      <Container>
        <Grid container>
          <Grid item xs={12} lg={3} md={3} sm={6}>
            <ProductFilter />
          </Grid>

          <Grid item xs={12} lg={9} md={9} sm={6}>
            <ProductsContent />
          </Grid>
        </Grid>
      </Container>


      <Footer />
    </Container >
  );
}

export default ProductsPage;
